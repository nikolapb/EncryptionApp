package com.android.nikolaprodanovic.encdecapplication.RC5;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.nikolaprodanovic.encdecapplication.R;


public class RC5Activity extends AppCompatActivity {

    String sadrzajZaEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("RC5Cipher");
        setContentView(R.layout.activity_encrypting_rc5);


        Button buttonEncrypt = (Button)findViewById(R.id.encrypt_button_rc5);

        buttonEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editText = (EditText) findViewById(R.id.edit_text_rc5);
                String textZaSifrovanje = editText.getText().toString().trim();

                EditText editKey = (EditText) findViewById(R.id.edit_key_rc5);
                String kljucZaSifrovanje = editKey.getText().toString().trim();

                TextView prikaziSifrat = (TextView) findViewById(R.id.text_view_set_rc5);

                String sifrovanTekst = "";

                if(textZaSifrovanje.isEmpty()){
                    editText.setError("Unesite text!");
                }
                else if(kljucZaSifrovanje.isEmpty()){
                    editKey.setError("Uneti ključ!");
                }
                else {
                    try {
                        sifrovanTekst = RC5Cipher.encrypt(textZaSifrovanje, kljucZaSifrovanje);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    prikaziSifrat.setText(sifrovanTekst);

                    sadrzajZaEmail = sifrovanTekst;
                }
            }
        });


        Button buttonDecrypt = (Button)findViewById(R.id.decrypt_button_rc5);

        buttonDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editText = (EditText) findViewById(R.id.edit_text_rc5);
                String textZaDesifrovanje = editText.getText().toString();

                EditText editKey = (EditText) findViewById(R.id.edit_key_rc5);
                String kljucZaDesifrovanje = editKey.getText().toString();

                TextView prikaziSifrat = (TextView) findViewById(R.id.text_view_set_rc5);

                String desifrovanTekst = "";

                if(textZaDesifrovanje.isEmpty()){
                    editText.setError("Unesite text!");
                }
                else if(kljucZaDesifrovanje.isEmpty()){
                    editKey.setError("Uneti ključ!");
                }
                else {
                    try {

                        desifrovanTekst = RC5Cipher.decrypt(textZaDesifrovanje, kljucZaDesifrovanje);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    prikaziSifrat.setText(desifrovanTekst);

                    sadrzajZaEmail = desifrovanTekst;
                }
            }
        });

        Button emailButton = (Button)findViewById(R.id.email_button_rc5);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_TEXT, sadrzajZaEmail);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }
}

