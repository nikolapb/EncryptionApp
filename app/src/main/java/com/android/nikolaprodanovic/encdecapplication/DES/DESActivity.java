package com.android.nikolaprodanovic.encdecapplication.DES;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.android.nikolaprodanovic.encdecapplication.R;


import static com.android.nikolaprodanovic.encdecapplication.DES.DESCipher.decrypt;
import static com.android.nikolaprodanovic.encdecapplication.DES.DESCipher.encrypt;


public class DESActivity extends AppCompatActivity {

    private String sifrat;
    EditText editKey;
    EditText editText;
    TextView prikaziSifrat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("DES");
        setContentView(R.layout.activity_encrypting_des);


        Button buttonEncrypt = (Button)findViewById(R.id.encrypt_button);

        buttonEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editText = (EditText) findViewById(R.id.edit_text);
                String textZaSifrovanje = editText.getText().toString().trim();

                editKey = (EditText) findViewById(R.id.edit_key);
                final String kljucZaSifrovanje = editKey.getText().toString().trim();


                if(textZaSifrovanje.isEmpty()){
                    editText.setError("Unesite text!");
                }
                else if(kljucZaSifrovanje.length()<8){
                    editKey.setError("Ključ mora biti dužine 8 karaktera!");
                }
                else {
                    prikaziSifrat = (TextView) findViewById(R.id.text_view_set);

                    String enkriptovanText = encrypt(textZaSifrovanje, kljucZaSifrovanje.getBytes());

                    prikaziSifrat.setText(enkriptovanText);

                    sifrat = enkriptovanText;
                }
            }
        });


        Button buttonDecrypt = (Button)findViewById(R.id.decrypt_button);

        buttonDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editText = (EditText) findViewById(R.id.edit_text);
                String textDesifrovanje = editText.getText().toString().trim();

                editKey = (EditText) findViewById(R.id.edit_key);
                final String kljucZaDesifrovanje = editKey.getText().toString().trim();


                if(textDesifrovanje.isEmpty()){
                    editText.setError("Unesite text!");
                }
                else if(kljucZaDesifrovanje.length()<8){
                    editKey.setError("Ključ mora biti dužine 8 karaktera!");
                }
                else {
                    prikaziSifrat = (TextView) findViewById(R.id.text_view_set);

                    String dekriptovanText = decrypt(textDesifrovanje, kljucZaDesifrovanje.getBytes());

                    prikaziSifrat.setText(dekriptovanText);

                    sifrat = dekriptovanText;
                }
            }
        });


        Button emailButton = (Button)findViewById(R.id.email_button);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_TEXT,sifrat);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }

}
