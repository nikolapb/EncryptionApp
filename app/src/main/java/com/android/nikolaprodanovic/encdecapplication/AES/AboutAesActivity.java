package com.android.nikolaprodanovic.encdecapplication.AES;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.nikolaprodanovic.encdecapplication.R;

public class AboutAesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("About AES");
        setContentView(R.layout.activity_about);

        String aboutAes = "AES (engl. Advanced Encryption Standard, napredni standard za enkripciju) " +
                "je specifikacija za enkripciju elektronskih podataka. Usvojen je od strane Vlade SAD " +
                "i koristi se širom sveta. AES je zamenio prethodno korišćeni standard DES. " +
                "AES je algoritam simetričnog ključa, što znači da se isti ključ koristi i za enkripciju " +
                "i za dekripciju podataka.\n\nPrilikom objave poziva za ponude za AES, sledeći zahtevi su " +
                "postavljeni:\n" +
                "1) Blok šifra sa dužinom bloka od 128 bita;\n" +
                "2) Tri dužine ključa od 128, 192 i 256 bita;\n" +
                "3) Sigurnost na nivou ostalih predloženih algoritama;\n" +
                "4) Efikasnost u softveru i hardveru.\n\nUlaz i izlaz iz AES algoritma se sastoje od sekvenci " +
                "(blokova) sa po 128 bita. Broj bita " +
                "se naziva dužina sekvence ili bloka. Ključ za AES algoritam je sekvenca od 128, 192 " +
                "ili 256 bita. Svi drugi ulazi, izlazi ili ključevi u AES algoritam nisu dozvoljeni.\n\nIako je " +
                "tehnologija u poslednjoj deceniji puno napredovala, sa tendencijom rasta " +
                "ulaganja sredstava u poboljšanje performansi računarskih sistema, računajući na relativno " +
                "veliku dužinu ključa i često kratku vremensku validnost informacija koje se " +
                "kriptuju, od AES-a se očekuje da još dugo bude standard u kriptografiji. \n\nOtpornost " +
                "na brute-force metodu korektno implementirane 128 bitne varijante AES-a, protiv budžeta " +
                "od 1000000$, je osigurana na minimum 50 godina. \n\nOktobra 2000-te godine " +
                "Bruce Schneier, programer Twofish algoritma koji je bio konkurencija Rijndael-u u " +
                "izboru za AES, iako je u početku mislio da će uspešan akademski napad na Rijdnael " +
                "biti osmišljen jednog dana, na kraju je ipak rekao: \"Ne vjerujem da će iko ikad " +
                "uspeti da otkrije napad kojim će uspešno čitati Rijndael-ov saobraćaj\". \n\nS obzirom " +
                "na vreme u kome živimo, kada različiti subjekti pokušavaju da vrše kontrolu internet " +
                "saobraćaja, postojanje naprednih, jakih standarda enkripcije je veoma važno u cilju " +
                "očuvanja privatnosti komunikacije.";

        TextView textView = (TextView)findViewById(R.id.about_text_view);

        textView.setText(aboutAes);

    }
}
