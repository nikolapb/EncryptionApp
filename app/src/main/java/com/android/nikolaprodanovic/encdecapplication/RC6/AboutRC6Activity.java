package com.android.nikolaprodanovic.encdecapplication.RC6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.nikolaprodanovic.encdecapplication.R;

public class AboutRC6Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("About RC6");
        setContentView(R.layout.activity_about);

        String aboutRC6 = "RC6 algoritam se pojavio kao unapređenje RC5Cipher algoritma, naravno sa strožijim zahtevima po pitanju " +
                "sigurnosti i boljim performansama. Kao i kod RC5Cipher i ovaj algoritam koristi rotacije sa promenjivim pomakom. " +
                "Novo je jedino to što RC6 koristi četiri umesto dva bloka reči (radna registra). RC6 podržava blokove podataka od " +
                "po 128 bitova i koristi ključeve veličine 128, 192 i 256 bita.\n\nVarijante RC6 se specifikuju kao RC6-w/r/b gde " +
                "je w veličina reči u bitovima, r predstavlja broj koraka algoritma i b je dužina ključa u bajtovima.\n\n" +
                "Četiri w-bitna radna registra koja sadrže izvornu poruku su A, B, C i D. S[0,1,…, 2r + 3] predstavljaju w-bitne " +
                "delove ključa iz proširene tabele ključeva. Dobijeni kriptovani tekst se skladišti u registrima A, B, C i D. " +
                "Formiranje proširene tabele ključeva se vrši isto kao i kod RC5Cipher, pomoću konstanti Pw i Qw.\n\n" +
                "U avgustu 2016. otkriven je kod nazvan \"Equation Group\" ili NSA \"implementacije\" za različite mrežne sigurnosne uređaje. " +
                "Priložena uputstva otkrila su da neki od ovih programa koriste RC6 za poverljivost mrežnih komunikacija.";

        TextView textView = (TextView)findViewById(R.id.about_text_view);

        textView.setText(aboutRC6);
    }
}
