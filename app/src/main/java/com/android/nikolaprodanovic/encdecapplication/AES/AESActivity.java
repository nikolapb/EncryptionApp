package com.android.nikolaprodanovic.encdecapplication.AES;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import android.util.Base64;

import com.android.nikolaprodanovic.encdecapplication.R;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class AESActivity extends AppCompatActivity {

    private static String sadrzajZaEmail;
    EditText editText;
    EditText editKey;
    TextView prikaziSifrat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("AES");
        setContentView(R.layout.activity_aes);

        Button aesEncryptButton = (Button)findViewById(R.id.encrypt_button_aes);

        aesEncryptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  {

                editText = (EditText)findViewById(R.id.edit_text_aes);
                String textZaSifrovanje = editText.getText().toString().trim();

                editKey = (EditText)findViewById(R.id.edit_key_aes);
                final String kljucZaSifrovanje = editKey.getText().toString().trim();


                if(textZaSifrovanje.isEmpty()){
                    editText.setError("Unesite text!");
                }
                else if(kljucZaSifrovanje.length()<16){
                    editKey.setError("Ključ mora biti dužine 16 karaktera!");
                }
                else {
                    prikaziSifrat = (TextView) findViewById(R.id.text_view_set_aes);

                    String enkriptovanText = AESActivity.encrypt(textZaSifrovanje, kljucZaSifrovanje.getBytes());

                    prikaziSifrat.setText(enkriptovanText);

                    sadrzajZaEmail = enkriptovanText;

                }
            }
        });




        Button aesDecryptButton = (Button)findViewById(R.id.decrypt_button_aes);

        aesDecryptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editText = (EditText)findViewById(R.id.edit_text_aes);
                String textZaDesifrovanje = editText.getText().toString().trim();


                editKey = (EditText)findViewById(R.id.edit_key_aes);
                final String kljucZaDesifrovanje = editKey.getText().toString().trim();


                if(textZaDesifrovanje.isEmpty()){
                    editText.setError("Unesite text!");
                }
                else if(kljucZaDesifrovanje.length()<16){
                    editKey.setError("Ključ mora biti dužine 16 karaktera!");
                }
                else {
                    prikaziSifrat = (TextView) findViewById(R.id.text_view_set_aes);

                    String desifrovanText = AESActivity.decrypt(textZaDesifrovanje, kljucZaDesifrovanje.getBytes());

                    prikaziSifrat.setText(desifrovanText);

                    sadrzajZaEmail = desifrovanText;
                }
            }
        });

        Button aesEmailButton = (Button)findViewById(R.id.email_button_aes);

        aesEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_TEXT,new String(sadrzajZaEmail));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }



    public static String encrypt(String stringToEncrypt, byte[] key){

        try {

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            final String encryptedString = Base64.encodeToString(cipher.doFinal(stringToEncrypt.getBytes()), Base64.URL_SAFE);

            return encryptedString;

        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public static String decrypt(String stringToDecrypt, byte[] key){

        try {

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            final String decryptedString = new String(cipher.doFinal(Base64.decode(stringToDecrypt, Base64.URL_SAFE)));

            return decryptedString;
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}
