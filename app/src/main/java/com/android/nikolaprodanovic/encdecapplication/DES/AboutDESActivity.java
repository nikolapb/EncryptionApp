package com.android.nikolaprodanovic.encdecapplication.DES;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.nikolaprodanovic.encdecapplication.R;

public class AboutDESActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("About DES");
        setContentView(R.layout.activity_about);

        String aboutDES = "DES (engl. Data Encryption Standard) predstavlja simetrični " +
                "algoritam za kriptovanje blokovskog tipa, odnosno predstavlja direktnu upotrebu " +
                "blok-šifre (ECB mod). Kao ulaz u algoritam se koristi blok od 64-bita izvornog teksta i " +
                "56- bitni ključ. Izlaz iz algoritma je 64-bitni kriptovan tekst koji se dobija nakon 16 " +
                "iteracija koje se sastoje od identičnih operacija.\n\nKriptovanje pomoću DES algoritma se " +
                "sprovodi u nekoliko koraka. Prvo se bitovi ulaznog bloka dužine 64 bita permutuju početnom " +
                "permutacijom. Radi se o permutaciji koja jednostavno vrši zamenu mesta bitova. Permutovan ulazni " +
                "blok deli na dva dela od po 32 bita, levi L0 i desni R0 deo. Nad desnim delom bloka se obavlja " +
                "funkcija f(R0,K1), gde je R0 desnih 32 bita, a K1 je 48-bitni ključ. Ova funkcija generiše " +
                "32-bitni rezultat. Nad dobijenim rezultatom funkcije f i L0 vrši se operacija XOR. Rezultat " +
                "XOR operacije predstavlja novu 32-bitnu vrednost R1 koja se koristi za dalje operacije. Kao " +
                "levi deo L1 se koristi vrednost R0 iz prethodne iteracije. Nakon ponavljanja 16 istovetnih " +
                "koraka, blokovi međusobno menjaju mesta te se spajaju. Na kraju se obavlja konačna permutacija " +
                "koja je inverzna početnoj. Dobijena 64-bitna vrednost čini kriptovani blok podataka.\n\nPrilikom " +
                "kriptovanja/dekriptovanja u svakoj iteraciji se koriste različiti ključevi K1, ..., K16 veličine " +
                "48-bita. Za generisanje ovih ključeva se koristi poseban algoritam. Na Slici 3.1.2 ilustrovan je " +
                "postupak dobijanja ključeva koji se koriste prilikom DES kriptovanja/dekriptovanja.\n\nZanimljivo " +
                "je da se napomene jedna slabost vezana za DES algoritam. Naime, zbog samog načina kreiranja delova " +
                "ključeva, postoje četiri ključa za koje je dekripcija jednaka enkripciji. To znači, ako se kriptuje " +
                "neka poruka dva puta i to sa baš jednim od ta 4 ključa, kao rezultat se dobija originalna poruka. " +
                "Kako je verovatnoća da se od svih mogućih ključeva odabere baš neki od tih mala, to ne utiče na " +
                "sigurnost algoritma.";

        TextView textView = (TextView)findViewById(R.id.about_text_view);

        textView.setText(aboutDES);


    }
}
