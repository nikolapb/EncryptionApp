package com.android.nikolaprodanovic.encdecapplication.RC5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.android.nikolaprodanovic.encdecapplication.R;

public class AboutRC5Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("About RC5Cipher");
        setContentView(R.layout.activity_about);

        String aboutRC5 = "RC5 pripada simetričnim šifarskim blokovima pogodan kako za softversku tako i za " +
                "hardversku realizaciju. RC5 predstavlja algoritam sa promenjivom veličinom bloka ulaznih podataka, " +
                "18 promenjivim brojem koraka izvršavanja i promenjivim dužinama ključa. Na ovaj način RC5 obezbeđuje " +
                "veliku fleksibilnost u performansama i nivoima sigurnosti podataka.\n\nRC5 algoritam karakterišu sledeći " +
                "parametri RC5-w/r/b. Gde je w broj bitova u reči, r predstavlja broj koraka RC5 algoritma, a b je dužina " +
                "ključa izražena u bajtovima. Različitim izborom parametara dobijaju se različiti RC5 algoritmi. Standardnu " +
                "reč čini 32 bita, dok su moguće i vrednosti 16 bita i 64 bita. RC5 kriptovanje koristi blokove od dve reči (2w) " +
                "izvornog ili kriptovanog teksta. Moguće vrednosti paramera r su 0,1,…,255. Broj bajtova koji čine ključ, b, kreće " +
                "se od 0 do 255. Tajni ključ K se često pre upotrebe proširi i na taj način se formira takozvana proširena tabela " +
                "ključeva S.\n\nZbog jednostavnosti operacija RC5 je lak za implementaciju. Uz to iznos pomaka u operacijama rotiranja " +
                "nije fiksan, već zavisi od ulaznog podatka.";

        TextView textView = (TextView)findViewById(R.id.about_text_view);

        textView.setText(aboutRC5);

    }
}
