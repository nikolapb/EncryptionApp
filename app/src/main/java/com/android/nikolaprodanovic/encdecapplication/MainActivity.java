package com.android.nikolaprodanovic.encdecapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.nikolaprodanovic.encdecapplication.AES.AESActivity;
import com.android.nikolaprodanovic.encdecapplication.AES.AboutAesActivity;
import com.android.nikolaprodanovic.encdecapplication.DES.AboutDESActivity;
import com.android.nikolaprodanovic.encdecapplication.DES.DESActivity;
import com.android.nikolaprodanovic.encdecapplication.RC5.AboutRC5Activity;
import com.android.nikolaprodanovic.encdecapplication.RC5.RC5Activity;
import com.android.nikolaprodanovic.encdecapplication.RC6.AboutRC6Activity;
import com.android.nikolaprodanovic.encdecapplication.RC6.RC6Activity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button desButton = (Button)findViewById(R.id.des_button);


        desButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent desButtonIntent = new Intent(MainActivity.this,DESActivity.class);

                startActivity(desButtonIntent);
            }
        });


        Button caesarButton = (Button) findViewById(R.id.rc5_button);

        caesarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent caesarIntent = new Intent(MainActivity.this,RC5Activity.class);

                startActivity(caesarIntent);
            }
        });


        Button aesButton = (Button)findViewById(R.id.aes_button);

        aesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vigenereIntent = new Intent(MainActivity.this, AESActivity.class);

                startActivity(vigenereIntent);
            }
        });


        Button md5Button = (Button)findViewById(R.id.rc6_button);

        md5Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent md5Intent = new Intent(MainActivity.this,RC6Activity.class);

                startActivity(md5Intent);
            }
        });

        Button aboutXorButton = (Button)findViewById(R.id.about_xor_button);

        aboutXorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aboutXorIntent = new Intent(MainActivity.this,AboutDESActivity.class);

                startActivity(aboutXorIntent);
            }
        });

        Button aboutCaesarButton = (Button)findViewById(R.id.about_caesar_button);

        aboutCaesarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aboutCaesarIntent = new Intent(MainActivity.this,AboutRC5Activity.class);

                startActivity(aboutCaesarIntent);
            }
        });


        Button aboutAesButton = (Button)findViewById(R.id.about_aes_button);

        aboutAesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aboutAesIntent = new Intent(MainActivity.this,AboutAesActivity.class);

                startActivity(aboutAesIntent);
            }
        });

        Button aboutMD5button = (Button)findViewById(R.id.about_md5_button);

        aboutMD5button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aboutMD5Intent = new Intent(MainActivity.this,AboutRC6Activity.class);

                startActivity(aboutMD5Intent);
            }
        });

    }
}
